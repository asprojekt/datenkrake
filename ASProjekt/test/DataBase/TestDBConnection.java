package DataBase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import de.shooter.constants.DBConstants;
import de.shooter.constants.SQLConstants;
import de.shooter.database.DBHandler;
import de.shooter.database.DataManager;
import de.shooter.exception.ShooterException;

public class TestDBConnection {

	DataManager dm;
	
	@Before
	public void setup() {
		this.dm = new DataManager();
	}
	
	@Test(expected = ShooterException.class)
	public void testInvalidSQLOrder() {
		dm.getDataSetFromDBWhereParam("test", "test");
		Assert.fail();
	}
	
	@Test
	public void testValidSQLOrder() {
		Assert.assertNotNull(dm.getAllEntriesFromDB());
	}
	
	@Test
	public void testSQLWhereOrderClmArmed() {
		
		Assert.assertNotNull(dm.getDataSetFromDBWhereParam(DBConstants.DB_CLM_ARMED, "gun"));
	}
	
	@Test
	public void testSQLWhereOrderClmAge() {
		Assert.assertNotNull(dm.getDataSetFromDBWhereParam(DBConstants.DB_CLM_AGE, "22"));
	}
}
