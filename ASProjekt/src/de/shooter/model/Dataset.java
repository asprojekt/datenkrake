package de.shooter.model;

/**
 * Model-Klasse f�r den Datensatz.
 * 
 * @author Christopher.
 *
 */
public class Dataset {
	private int id;
	private String name;
	private String date;
	private String mannerOfDeath;
	private String armed;
	private int age;
	private String gender;
	private String race;
	private String city;
	private String state;
	private String signsOfMentalIllness;
	private String threadLevel;
	private String flee;
	private String bodyCamera;
	private String armsCategory;

	/**
	 * Konstruktor.
	 * 
	 * @param id
	 * @param name
	 * @param date
	 * @param mannerOfDeath
	 * @param armed
	 * @param age
	 * @param gender
	 * @param race
	 * @param city
	 * @param state
	 * @param signsOfMentalIllness
	 * @param threadLevel
	 * @param flee
	 * @param bodyCamera
	 * @param armsCategory
	 */
	public Dataset(int id, String name, String date, String mannerOfDeath, String armed, int age, String gender,
			String race, String city, String state, String signsOfMentalIllness, String threadLevel, String flee,
			String bodyCamera, String armsCategory) {
		this.id = id;
		this.name = name;
		this.date = date;
		this.mannerOfDeath = mannerOfDeath;
		this.armed = armed;
		this.age = age;
		this.gender = gender;
		this.race = race;
		this.city = city;
		this.state = state;
		this.signsOfMentalIllness = signsOfMentalIllness;
		this.threadLevel = threadLevel;
		this.flee = flee;
		this.bodyCamera = bodyCamera;
		this.armsCategory = armsCategory;
	}

	// GETTER
	public int getId() {
		return id;
	};

	public String getName() {
		return name;
	};

	public String getDate() {
		return date;
	};

	public String getMannerOfDeath() {
		return mannerOfDeath;
	};

	public String getArmed() {
		return armed;
	}

	public int getAge() {
		return age;
	};

	public String getGender() {
		return gender;
	};

	public String getRace() {
		return race;
	};

	public String getCity() {
		return city;
	};

	public String getState() {
		return state;
	};

	public String getSignsOfMentalIllness() {
		return signsOfMentalIllness;
	};

	public String getThreadLevel() {
		return threadLevel;
	};

	public String getFlee() {
		return flee;
	};

	public String getBodyCamera() {
		return bodyCamera;
	};

	public String getArmsCategory() {
		return armsCategory;
	};
}
