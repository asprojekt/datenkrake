package de.shooter.fakten;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Diese Klasse enth�llt die Implementierung der t�glichen Fakten.
 * @author Garip
 *
 */
public class Facts {

//	public static void main(String[] args) throws IOException {
//		// TODO Auto-generated method stub
//		Timer timer = new Timer();
//
//		timer.schedule( new TimerTask() {
//		    public void run() {
//		    	
//		    	getRandomFacts();
//		    	
//		    }
//		 }, 0, 3000);
//		getRandomFacts();
//		
//	}
	
	public String getRandomFacts() {
		System.out.flush(); 
		BufferedReader buffer = null;
		String line;
		String zufallsZeile = "";
		List<String> list = new ArrayList<String>();
		try {
			buffer = new BufferedReader(new InputStreamReader(new FileInputStream("src/de/shooter/fakten/fakten.txt")));
			while ((line = buffer.readLine()) != null) 
			list.add(line);
			zufallsZeile = list.get(new java.util.Random().nextInt(list.size()));
			System.out.println(zufallsZeile);			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return zufallsZeile;
	}
	

}
