package de.shooter.exception;

public class ShooterException extends RuntimeException{

	/**
	 * RuntimeException f�r das Programm Shooter.
	 */
	private static final long serialVersionUID = 1L;

	public ShooterException(String message) {
		super(message);
	}
	
}
