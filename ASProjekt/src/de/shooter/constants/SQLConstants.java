package de.shooter.constants;

public class SQLConstants {

	/** SELECT * FROM datensatz WHERE {0} = {1} */
	public final static String SQL_BASICORDER = "SELECT * FROM datensatz WHERE {0} = {1}";
	
	/** select * from datensatz */ 
	public final static String SQL_GETALL = "select * from datensatz";
	
}
