package de.shooter.constants;

public class DBConstants {

	//?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC //Einf�gen, falls eine TImezone SQL Error kommt.
	public final static String DB_URL = "jdbc:mysql://localhost:3306/as_shooter?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	public final static String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
	
	public static final String DB_USER = "user";
	public static final String DB_PASSWORD = "passwort";
	
	public static final String DB_CLM_ID = "id";
	public static final String DB_CLM_NAME = "name";
	public static final String DB_CLM_DATE = "date";
	public static final String DB_CLM_MANNER_OF_DEATH = "manner_of_death";
	public static final String DB_CLM_ARMED = "armed";
	public static final String DB_CLM_AGE = "age";
	public static final String DB_CLM_GENDER = "gender";
	public static final String DB_CLM_RACE = "race";
	public static final String DB_CLM_CITY = "city";
	public static final String DB_CLM_STATE = "state";
	public static final String DB_CLM_SIGNS_OF_MENTAL_ILLNESS = "signs_of_mental_illness";
	public static final String DB_CLM_THREAT_LEVEL = "threat_level";
	public static final String DB_CLM_FLEE = "flee";
	public static final String DB_CLM_BODY_CAMERA = "body_camera";
	public static final String DB_CLM_ARMS_CATEGORY = "arms_category";

}