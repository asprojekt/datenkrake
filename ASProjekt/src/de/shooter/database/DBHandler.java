package de.shooter.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import de.shooter.constants.DBConstants;
import de.shooter.constants.SQLConstants;
import de.shooter.exception.ShooterException;
import de.shooter.model.Dataset;

/**
 * Diese Klasse enthällt die DAtenbankverbindung.
 * 
 * @author Philipp
 *
 */
public class DBHandler {

	/**
	 * Aufruf der Datenbank mit einem beliebigen SQL SELECT * Befehl.
	 * @param sqlOrder Muss ein SELECT * Befehl sein (Einschränkungsparameter sind möglich)
	 * @return Liste der gefundenen Datensätze.
	 */
	public ArrayList<Dataset> getDatasetFromDB(String sqlOrder) {
		try {
			Class.forName(DBConstants.DB_DRIVER);
			
		} catch (ClassNotFoundException e) {
			throw new ShooterException(e.getMessage());
		}
		

		try (Connection connect = DriverManager.getConnection(DBConstants.DB_URL, DBConstants.DB_USER,
				DBConstants.DB_PASSWORD); Statement st = connect.createStatement()) {
			
			ResultSet res = st.executeQuery(sqlOrder);
			ArrayList<Dataset> dataList = new ArrayList<Dataset>();
			while(res.next()) {			
			dataList.add( new Dataset(
					res.getInt(DBConstants.DB_CLM_ID),
					res.getString(DBConstants.DB_CLM_NAME),
					res.getString(DBConstants.DB_CLM_DATE),
					res.getString(DBConstants.DB_CLM_MANNER_OF_DEATH),
					res.getString(DBConstants.DB_CLM_ARMED),
					res.getInt(DBConstants.DB_CLM_AGE),
					res.getString(DBConstants.DB_CLM_GENDER),
					res.getString(DBConstants.DB_CLM_RACE),
					res.getString(DBConstants.DB_CLM_CITY),
					res.getString(DBConstants.DB_CLM_STATE),
					res.getString(DBConstants.DB_CLM_SIGNS_OF_MENTAL_ILLNESS),
					res.getString(DBConstants.DB_CLM_THREAT_LEVEL),
					res.getString(DBConstants.DB_CLM_FLEE),
					res.getString(DBConstants.DB_CLM_BODY_CAMERA),
					res.getString(DBConstants.DB_CLM_ARMS_CATEGORY)
					));
			}

			return dataList;
			
		} catch (SQLException e) {
			throw new ShooterException(e.getMessage());
		}

	}

}
