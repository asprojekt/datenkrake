package de.shooter.database;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import de.shooter.constants.SQLConstants;
import de.shooter.model.Dataset;

/**
 * Die Klasse nutzen wir um eventuelle Logik nicht in den ActionListener der Butons implementieren zu m�ssen.
 * 
 * @author Philipp
 *
 */
public class DataManager {

	private DBHandler db;
	
	/**
	 * Kosntruktor.
	 */
	public DataManager() {
		this.db = new DBHandler();
	}
	
	/**
	 * Gibt alle gefunden Datens�tze zur�ck.
	 * @return Liste der gefundenen Datens�tze.
	 */
	public List<Dataset> getAllEntriesFromDB(){
		return this.db.getDatasetFromDB(SQLConstants.SQL_GETALL);
	}
	
	/**
	 * Gibt alle gefundenen Datens�tze zur�ck, die der �bergeben PArameter entsprechen.
	 * @param dbColum Spalte der Datenbank, f�r die WHERE bedingung.
	 * @param param Wert der f�r die WHERE Bedingung genutzt wird.
	 * @return Liste der gefundenen Datens�tze.
	 */
	public List<Dataset> getDataSetFromDBWhereParam(String dbColum, String param){
		return this.db.getDatasetFromDB(MessageFormat.format(SQLConstants.SQL_BASICORDER, dbColum, "'" + param + "'"));
	}
}
