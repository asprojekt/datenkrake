package application;
	
import de.shooter.constants.SQLConstants;
import de.shooter.database.DBHandler;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

/**
 * Start des Programms.
 * @author Darwin.
 *
 */
public class Start extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			Pane root =  FXMLLoader.load(getClass().getResource("asgui.fxml"));
			Scene scene = new Scene(root, 1280, 720);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();	
			primaryStage.setOnCloseRequest(e -> {
				System.exit(0);
			});
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
