package application;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.shooter.fakten.Facts;
import de.shooter.model.Dataset;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AppController {

	@FXML
	private Pane fakten;
	@FXML
	private Label faktenText;
	@FXML
	private VBox menu;
	@FXML
	private HBox ueberschrift;
	@FXML
	private Label ueberschriftLabel;
	@FXML
	private AnchorPane content;
	@FXML
	private AnchorPane pane_saulendiagramm;
	@FXML
	private BarChart<Number, String> saulendiagramm;
	@FXML
	private AnchorPane pane_kreisdiagramm;
	@FXML 
	private PieChart kreisdiagramm;
	@FXML
	private AnchorPane pane_tabelle;
	@FXML
	private TableView<Dataset> tabelle;
	
	private Facts factGenerator = new Facts();
	
	public void initialize() {
		fillTable();
		startBanner();
//		FaktenBanner task = new FaktenBanner(fakten, faktenText);
//		ExecutorService executorService = Executors.newFixedThreadPool(1);
//		executorService.execute(task);
//		executorService.shutdown();
	}
	
	private void startBanner() {
		faktenText.setText(factGenerator.getRandomFacts());
		TranslateTransition tt = new TranslateTransition(Duration.millis(14000), faktenText);
//    	Stage st = new Stage();
//    	st = (Stage) faktenText.getScene().getWindow();
		tt.setFromX(250);
    	tt.setToX(-2000);
    	tt.setCycleCount(1);
    	tt.setAutoReverse(false);
    	tt.play();
    	tt.setOnFinished(e -> {
    		faktenText.setText(factGenerator.getRandomFacts());
    		startBanner();
    	});
	}

	public void fillTable() {
		TableColumn<Dataset, String> UserId = new TableColumn<Dataset, String>();
		UserId.setCellValueFactory(new PropertyValueFactory<Dataset, String>("id"));
		tabelle.getColumns().add(UserId);
	}
	
	@FXML
	public void waffenClicked(Event e) {
		ueberschriftLabel.setText("Waffen");
	}
	
	@FXML
	public void personenClicked(Event e) {
		ueberschriftLabel.setText("Personen");
	}
	
	@FXML
	public void orteClicked(Event e) {
		ueberschriftLabel.setText("Orte");
	}
}
