package application;

import java.util.Timer;
import java.util.TimerTask;

import javafx.animation.TranslateTransition;
import javafx.concurrent.Task;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class FaktenBanner extends Task<String>{

	private Pane banner;
	private Label label;
	
	public FaktenBanner(Pane banner, Label label) {
		this.banner = banner;
		this.label = label;
	}
	
	@Override
	protected String call() throws Exception {
		int i = 0;
		new Timer().schedule(
			    new TimerTask() {

			        @Override
			        public void run() {
//			        	label.setLayoutX(label.getLayoutX()-2);
			        	TranslateTransition tt = new TranslateTransition(Duration.millis(5000), label);
			        	Stage st = new Stage();
			        	st = (Stage) label.getScene().getWindow();
			        	tt.setFromX(st.getWidth());
			        	tt.setToX(0-label.getWidth());
			        	tt.play();
			        	tt.setOnFinished(e -> {
			        		
			        	});
			        }
			    }, 0, 17);	
		return null;
	}

}
